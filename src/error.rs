/// This Error is given when failing to get a pretty representation of a table
/// using the [`Table.get_pretty`] command.
#[derive (Debug, Clone, Copy)]
pub enum PrettyError {
    /// This is when the available width is zero.
    /// If this is the case then it is impossible to
    /// determine cell width.
	ZeroedWidth,
	/// This referes to when a width _is_ given but
	/// even so after the division there is 0 width.
	///
	/// For example, given a table of 80 elements and a width of 81
	/// there just isn't space to print it.
	NoSpaceWidth,
}


