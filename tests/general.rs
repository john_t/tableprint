use table_print::Table;

// This is just to generate a simple tabl
fn gen_table() -> Table {
    // Creates a table
	let mut table = Table::new(
    	vec![
        	"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"
    	].into_iter().map(String::from).collect()
	);

	// Inserts some rows
	table.insert_row(
		vec![
    		"This", "Row", "Has", "A Chance", "To", "Overflow!"
    	].into_iter().map(String::from).collect()
	);

	table.insert_row(
		vec![
			"This", "Row", "Is", "Very", "Boring"
		].into_iter().map(String::from).collect()
	);

	table.insert_row(
		vec![
			"This",
			"Row",
			"Could",
			"Possibly",
			"WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP WRAP"
		].into_iter().map(String::from).collect()
	);

	table.insert_row(
		vec![
			"This", "Row", "Could", "Be", "Problematic \n ,, \" \" , <br> &lt &gt |"
		].into_iter().map(String::from).collect()
	);

	// Return the table
	table
}

#[test]
fn print_test() {
	let table = gen_table();
	println!("{}", table.get_pretty(80).unwrap());
	assert_eq!(
    	include_str!("./outputs/test.txt"),
    	table.get_pretty(80).unwrap(),
	);
	
}

#[test]
fn html_test() {
	let table = gen_table();
	println!("{}", table.get_html(None));
	assert_eq!(
    	include_str!("./outputs/test.html"),
    	table.get_html(None),
	);
}

#[test]
fn csv_test() {
	let table = gen_table();
	println!("{}", table.get_csv());
	assert_eq!(
    	table.get_csv(),
    	include_str!("./outputs/test.csv")
	);
}
