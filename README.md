# TablePrint

[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

TablePrint is a small library designed so that you can easily create tables.
Some of the notable features include:

 * Export to HTML
 * Export to CSV
 * Export to Plaintext _with_ wrap.

You can easily create tables like this:

```rust
// Create a table with the headings of "Person", "History Grade" and "Geography Grade"
let table = Table::new(vec!["Person", "History Grade", "Geography Grade"]);

// Add Some Rows
table.add_row(vec!["John", "93", "92"]);
table.add_row(vec!["Anne", "98", "87"])

// Export to plaintext and print.
// 80 for 80 columns.
println!("Here are the grades: \n\n{}", table.get_pretty(80));
```
